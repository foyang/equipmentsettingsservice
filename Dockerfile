FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/equipermentSettingsService-0.0.1-SNAPSHOT.jar equipermentSettingsService-0.0.1-SNAPSHOT.jar
EXPOSE 9001
COPY . .
ENTRYPOINT ["java", "-jar","equipermentSettingsService-0.0.1-SNAPSHOT.jar"]