package ooka.equipermentSettingsService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EquipermentSettingsServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(EquipermentSettingsServiceApplication.class, args);
	}
}
