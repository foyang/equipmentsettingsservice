package ooka.equipermentSettingsService.restController;

import io.github.resilience4j.retry.annotation.Retry;
import ooka.equipermentSettingsService.payload.Equipment;
import ooka.equipermentSettingsService.payload.FuelSystem;
import ooka.equipermentSettingsService.payload.OilSystem;
import ooka.equipermentSettingsService.payload.Valid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@CrossOrigin
@RequestMapping("service-equipment")
public class ServiceCommunicationController {
    private static final String FUELSERVICE = "fullback_retrymmm";
    @Value("${config.service.timout}")
    private Long duration;
    private String currentService = "";
    RestTemplate restTemplate = new RestTemplate();

    @PostMapping("add")
    public ResponseEntity<Equipment> saveEqupment(@RequestBody Equipment equipment){
        try {
            Thread.sleep(duration);
            HttpEntity<Equipment> request = new HttpEntity<>(equipment);
            restTemplate.postForEntity("http://equipment-persist-service:9002/equipment/add", request, Equipment.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("valid/oil")
    @Retry(name = FUELSERVICE, fallbackMethod = "fullback_retry_valid")
    public ResponseEntity<Valid> checkValidOil(@RequestBody OilSystem oilSystem){
        currentService = "oil";
        try {
            Thread.sleep(duration);
            HttpEntity<OilSystem> request = new HttpEntity<>(oilSystem);
            return restTemplate.postForEntity("http://oilsystem-service:9003/oil-system/valid",request, Valid.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    return null;
    }

    @PostMapping("valid/fuel")
    @Retry(name = FUELSERVICE, fallbackMethod = "fullback_retry_valid")
    public ResponseEntity<Valid> checkValidFuel(@RequestBody FuelSystem fuelSystem){
        currentService = "fuel";
        try {
            Thread.sleep(duration);
            HttpEntity<FuelSystem> request = new HttpEntity<>(fuelSystem);
            return restTemplate.postForEntity("http://fuelsystem-service:9004/fuel-system/valid",request,Valid.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResponseEntity<String> fullback_retry_valid(Exception e){
        if (currentService.equals("oil")){
            return new ResponseEntity<String>("The Oil service is down or not responding!!",HttpStatus.REQUEST_TIMEOUT);
        } else {
            return new ResponseEntity<String>("The Fuel service is down or not responding!!",HttpStatus.REQUEST_TIMEOUT);
        }
    }

    @Retry(name = FUELSERVICE, fallbackMethod = "fullback_retry")
    @GetMapping("test-service")
    public String testServiceCircuitBreaker(){
        return restTemplate.getForObject("http://fuelsystem-service:9004/fuel-system/test", String.class);
    }

    public String fullback_retry(Exception e){
        return "The service is down or not responding!!";
    }

    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
