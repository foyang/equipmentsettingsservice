package ooka.equipermentSettingsService.repository;


import ooka.equipermentSettingsService.model.OptionEquipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OptionEquipmentRepository extends JpaRepository<OptionEquipment,Integer> {
    OptionEquipment findById(int id);
    OptionEquipment findByName(String name);
}
