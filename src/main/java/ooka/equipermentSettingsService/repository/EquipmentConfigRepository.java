package ooka.equipermentSettingsService.repository;

import ooka.equipermentSettingsService.model.EquipmentConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EquipmentConfigRepository extends JpaRepository<EquipmentConfig,Integer> {
    EquipmentConfig findById(int id);
    List<EquipmentConfig> findByValue(String value);
}
